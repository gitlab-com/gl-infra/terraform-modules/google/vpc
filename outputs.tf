output "name" {
  value       = google_compute_network.main.name
  description = "The name of the network"
}

output "self_link" {
  value       = google_compute_network.main.self_link
  description = "The URL of the created network"
}

output "gateway_ipv4" {
  value       = google_compute_network.main.gateway_ipv4
  description = "The IPv4 address of the gateway"
}
