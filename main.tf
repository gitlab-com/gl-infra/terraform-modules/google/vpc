# Main VPC network for each environment
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_network
resource "google_compute_network" "main" {
  project                  = var.project
  name                     = local.name
  description              = local.description
  auto_create_subnetworks  = false
  enable_ula_internal_ipv6 = var.enable_internal_ipv6
}
