# Allow internal traffic
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall
resource "google_compute_firewall" "allow-internal" {
  count = length(var.internal_subnets) > 0 ? 1 : 0

  project = var.project
  name    = "allow-internal-${local.name}"
  network = google_compute_network.main.self_link

  allow {
    protocol = "all"
  }

  source_ranges = var.internal_subnets
}
