# GitLab.com VPC Terraform Module

## What is this?

This module provisions a VPC in GCP.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

## Internal IPv6 Range

You can read more about the ULA IPv6 range in this [RFC](https://datatracker.ietf.org/doc/html/rfc4193).
See the Google Cloud [documentation](https://cloud.google.com/vpc/docs/subnets#ipv6-ranges) for more information on internal IPv6 networking.

> After a VPC network has been assigned a /48 ULA IPv6 range, you can't remove or change the /48 ULA IPv6 range.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.9 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 5.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 5.0.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_compute_firewall.allow-internal](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_network.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_network) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_description"></a> [description](#input\_description) | n/a | `string` | `""` | no |
| <a name="input_enable_internal_ipv6"></a> [enable\_internal\_ipv6](#input\_enable\_internal\_ipv6) | Whether or not to automatically assign a /48 ULA IPv6 range to the VPC network. After a VPC network has been assigned a /48 ULA IPv6 range, you cannot remove or change the /48 ULA IPv6 range. | `bool` | `false` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | DEPRECATED, use name instead. The environment name | `string` | `""` | no |
| <a name="input_internal_subnets"></a> [internal\_subnets](#input\_internal\_subnets) | n/a | `list(string)` | <pre>[<br/>  "10.0.0.0/8"<br/>]</pre> | no |
| <a name="input_name"></a> [name](#input\_name) | The network name | `string` | `""` | no |
| <a name="input_project"></a> [project](#input\_project) | The project name | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_gateway_ipv4"></a> [gateway\_ipv4](#output\_gateway\_ipv4) | The IPv4 address of the gateway |
| <a name="output_name"></a> [name](#output\_name) | The name of the network |
| <a name="output_self_link"></a> [self\_link](#output\_self\_link) | The URL of the created network |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
