variable "project" {
  type        = string
  description = "The project name"
}

variable "name" {
  type        = string
  description = "The network name"
  default     = ""
}

variable "description" {
  type    = string
  default = ""
}

variable "internal_subnets" {
  type    = list(string)
  default = ["10.0.0.0/8"]
}

variable "enable_internal_ipv6" {
  description = "Whether or not to automatically assign a /48 ULA IPv6 range to the VPC network. After a VPC network has been assigned a /48 ULA IPv6 range, you cannot remove or change the /48 ULA IPv6 range."
  type        = bool
  default     = false
}

variable "environment" {
  type        = string
  description = "DEPRECATED, use name instead. The environment name"
  default     = ""
}
