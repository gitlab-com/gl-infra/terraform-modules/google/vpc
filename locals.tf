locals {
  name        = length(var.name) > 0 ? var.name : var.environment
  description = length(var.description) > 0 ? var.description : length(var.environment) > 0 ? "Main VPC for the ${var.environment} environment" : ""
}
